# Qué es este proyecto
El proyecto consiste en un pequeño programa realizado con Spring Boot que expone un API de compra de productos. 
Permite ver productos, añadir productos al carrito y realizar la compra. Todo ficticio claro. 

El proyecto tiene embebida una base de datos hsqldb y un servidor tomcat, así que no tiene dependencias externas.
El proyecto aparece con unos pocos productos.

También tiene instalado swagger. 

# Instrucciones
El proyecto está pensado para que cada uno lo descargue y ejecute en su máquina.

```bash
1. git clone git@gitlab.com:Canos/cart-testing.git
2. mvn spring-boot:run (o arrancar desde el IDE)
```


# Versión online
Hay una versión online disponible  
API: http://shop.david-canos.net  
Documentation: http://shop.david-canos.net/swagger-ui/#/  
OpenAPI spec : http://shop.david-canos.net/v3/api-docs
Swagger 2 spec : http://shop.david-canos.net/v2/api-docs 