package com.example.demorest;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.containsStringIgnoringCase;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TestingWebApplicationTest {

	@Autowired
	private MockMvc mockMvc;

    @Test
    public void shouldReturn_products() throws Exception {
        this.mockMvc.perform(get("/product"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsStringIgnoringCase("zapatilla")));
    }

    @Test
    public void shouldReturn_product_success() throws Exception {
        this.mockMvc.perform(get("/product/3"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("Citizen")));
    }

    @Test()
    public void shouldReturn_product_fail() throws Exception {
        this.mockMvc.perform(get("/product/6"))
                .andDo(print())
                .andExpect(status().is4xxClientError());

    }
}