package com.example.demorest.catalog;


import com.example.demorest.cart.repository.CartProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class ProductService {

    @Autowired private ProductRepository productRepository;
    @Autowired private CartProductRepository cartProductRepository;

    public ProductResponse findById(Integer idProduct) {
        final Product product = productRepository.findById(idProduct).orElseThrow(() -> new ProductNotFoundException());
        return ProductResponse.of(product);
    }

    public List<ProductResponse> findAll() {
        final List<Product> all = productRepository.findAll();
        return ProductResponse.of(all);
    }

    public void create(String name, BigDecimal price, Integer stock) {
        List<Product> others = productRepository.findByName(name);
        if(others.size() > 0) {
            throw new RuntimeException("Ya hay un producto con el mismo nombre");
        }

        Product p = new Product();
        p.setName(name);
        p.setPrice(price);
        p.setStock(stock);
        productRepository.save(p);
    }

    public void updatePrice(Integer idProduct, BigDecimal price) {
        final Product p = productRepository.findById(idProduct).orElseThrow();
        if(p.getPrice().compareTo(price) == 0) {
            throw new RuntimeException("Same price");
        }

        p.setPrice(price);
        productRepository.save(p);
    }

    public void updateName(Integer idProduct, String name) {
        final Product p = productRepository.findById(idProduct).orElseThrow();
        if(!productRepository.findByName(name).isEmpty()) {
            throw new RuntimeException("Same name in other product");
        }
        p.setName(name);
        productRepository.save(p);
    }

    public void updateStock(Integer idProduct, Integer stock) {
        final Product p = productRepository.findById(idProduct).orElseThrow();
        p.setStock(stock);
        productRepository.save(p);
    }

    public void reduceStock(Integer idProduct, Integer quantity) {
        final Product p = productRepository.findById(idProduct).orElseThrow();
        p.setStock(p.getStock() - quantity);
        productRepository.save(p);
    }

    public boolean checkStock(Integer idProduct, Integer quantity) {
        final Product p = productRepository.findById(idProduct).orElseThrow();
        return p.getStock() >= quantity;
    }


}
