package com.example.demorest.catalog;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
public class ProductController {

    @Autowired private ProductService productService;


    @ApiOperation(value = "See all product List", response = ProductResponse.class, responseContainer = "List")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "List with content"),
            @ApiResponse(code = 204, message = "No content"),
    })
    @GetMapping("/product")
    public ResponseEntity<List<ProductResponse>> product_list() {
        final List<ProductResponse> products = productService.findAll();

        return new ResponseEntity<>(products, products.size() > 0 ? HttpStatus.OK : HttpStatus.NO_CONTENT);
    }


    @ApiOperation(value = "See a product", response = Product.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Product")
    })
    @GetMapping("/product/{idProduct}")
    public ResponseEntity<ProductResponse> product_detail(@ApiParam(value = "Product Id", required = true) @PathVariable Integer idProduct) {
        ProductResponse p = productService.findById(idProduct);
        return new ResponseEntity<>(p, HttpStatus.OK);
    }

    @PostMapping("/product")
    public void create_product(@ApiParam(value = "Product name", required = true) @RequestParam String name,
                               @ApiParam(value = "Product price", required = true) @RequestParam BigDecimal price,
                               @ApiParam(value = "Stock available", required = true) @RequestParam Integer stock) {
        productService.create(name, price, stock);
    }

    @ApiOperation(value = "Update a product name")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success update"),
            @ApiResponse(code = 404, message = "Product does not exists")
    })
    @PutMapping("/product/{idProduct}/name")
    public void update_name(@ApiParam(value = "Product Id", required = true) @PathVariable Integer idProduct, @RequestParam String name) {
        productService.updateName(idProduct, name);
    }

    @ApiOperation(value = "Update a product Price")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success update"),
            @ApiResponse(code = 404, message = "Product does not exists")
    })
    @PutMapping("/product/{idProduct}/price")
    public void update_price(@ApiParam(value = "Product Id", required = true) @PathVariable Integer idProduct, @RequestParam BigDecimal price) {
        BigDecimal oldPrice = productService.findById(idProduct).getPrice();

        productService.updatePrice(idProduct, price);

        // BAJADA DE PRECIO
        if(oldPrice.compareTo(price) > 0) {
             // Realizar tareas de promoción
            // Avisar a los clientes interesados en "bajadas de precio"
        }
    }


    @ApiOperation(value = "Update a product stock")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success update"),
            @ApiResponse(code = 404, message = "Product does not exists")
    })
    @PutMapping("/product/{idProduct}/stock")
    public void update_stock(@ApiParam(value = "Product Id", required = true) @PathVariable Integer idProduct, @RequestParam Integer stock) {
        boolean was_soldout = !productService.checkStock(idProduct, 1);


        productService.updateStock(idProduct, stock);

        // El producto pasa de sin stock a con stock
        if(was_soldout && stock > 0) {
            // Avisar a los clientes que tenían este producto en la waiting list o en la wish list.
        }
    }
}
