package com.example.demorest.catalog;

import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

public class ProductResponse {

    private Integer idProduct;

    @ApiModelProperty(
            value = "product name",
            name = "name",
            dataType = "String",
            example = "Zapatillas Asycs")
    private String name;

    private BigDecimal price;
    private Integer stock;

    public static List<ProductResponse> of(List<Product> all) {
        return all.stream().map(p -> of(p)).collect(Collectors.toList());
    }

    public static ProductResponse of(Product product) {
        ProductResponse pr = new ProductResponse();
        pr.setIdProduct(product.getIdProduct());
        pr.setName(product.getName());
        pr.setPrice(product.getPrice());
        pr.setStock(product.getStock());
        return pr;
    }


    public Integer getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Integer idProduct) {
        this.idProduct = idProduct;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }
}
