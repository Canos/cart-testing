package com.example.demorest.orders;

import com.example.demorest.cart.model.CartProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class OrderService {

    @Autowired  private OrderRepository orderRepository;

    public Order create(Integer idCart, List<CartProduct> products, BigDecimal total) {
        Order order = new Order();
        order.setProducts(products.stream()
                .map(cp -> cp.getProduct().getName())
                .distinct().collect(Collectors.joining()));
        order.setNumOfProducts(products.size());
        order.setReference(String.valueOf(new Date().getTime()));
        order.setPurchaseDate(new Date());
        order.setAmount(total);
        order.setPhase(1);

        return orderRepository.save(order);
    }

    public void process(Integer idOrder) {
        final Order order = orderRepository.findById(idOrder).orElseThrow();
        order.setPhase(2);
        orderRepository.save(order);
    }

    public List<Order> findAll() {
        return orderRepository.findAll();
    }

    public Order findById(Integer idOrder) {
        return orderRepository.findById(idOrder).orElseThrow();
    }
}
