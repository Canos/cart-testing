package com.example.demorest.orders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class OrderController {

    @Autowired private OrderService orderService;

    @GetMapping(value="/order", headers = "Bearer")
    public List<Order> getAll() {
        return orderService.findAll();
    }

    @GetMapping(value="/order/{idOrder}", headers = "Bearer")
    public Order findById(@PathVariable Integer idOrder) {
        return orderService.findById(idOrder);
    }
}
