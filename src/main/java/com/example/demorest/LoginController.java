package com.example.demorest;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class LoginController {


    @ApiOperation(value="Login", nickname = "login")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "List with content", responseHeaders = {
                    @ResponseHeader(name = "Bearer", response = String.class)
            }),
            @ApiResponse(code = 204, message = "No content"),
    })
    @PostMapping()
    public ResponseEntity<String> login(@RequestParam String username, @RequestParam String pass) {

        boolean loginOk = (username.startsWith("admin"));

        if(loginOk) {
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Bearer", "auth-token-generated-by-login-system");

            return new ResponseEntity<>(responseHeaders, HttpStatus.OK);
        }
        else {
            throw new RuntimeException("Invalid credentials");
        }
    }

}
