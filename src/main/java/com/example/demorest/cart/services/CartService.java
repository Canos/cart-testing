package com.example.demorest.cart.services;

import com.example.demorest.cart.model.Cart;
import com.example.demorest.cart.model.CartProduct;
import com.example.demorest.cart.repository.CartProductRepository;
import com.example.demorest.cart.repository.CartRepository;
import com.example.demorest.cart.response.CartResponse;
import com.example.demorest.catalog.Product;
import com.example.demorest.catalog.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class CartService {

    @Autowired private CartRepository cartRepository;
    @Autowired private CartProductRepository cartProductRepository;
    @Autowired private ProductRepository productRepository;

    public CartResponse findCart(String sessionId) {
        Cart cart = cartRepository.findBySessionId(sessionId);
        return CartResponse.of(cart);
    }

    public CartResponse create(String id) {
        Cart cart = new Cart();
        cart.setSessionId(id);
        cart = cartRepository.save(cart);

        return CartResponse.of(cart);
    }

    public void addProduct(Integer idCart, Integer idProduct, Integer quantity) {
        final CartProduct cp = cartProductRepository.findByCart_idCartAndProduct_idProduct(idCart, idProduct);

        // crear producto en carrito
        if(cp == null) {
            CartProduct cp_new = new CartProduct();
            Product p = productRepository.findById(idProduct).orElseThrow();
            Cart c = cartRepository.findById(idCart).orElseThrow();
            cp_new.setProduct(p);
            cp_new.setCart(c);
            cp_new.setQuantity(quantity);
            cartProductRepository.save(cp_new);
        }
        // actualizar la cantidad
        else {
            cp.setQuantity(cp.getQuantity() + quantity);
            cartProductRepository.save(cp);
        }
    }

    public void empty(Integer idCart) {
        cartProductRepository.deleteByCart_idCart(idCart);
    }
}
