package com.example.demorest.cart.repository;

import com.example.demorest.cart.model.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CartRepository extends JpaRepository<Cart, Integer> {

    Cart findBySessionId(String sessionId);
}
