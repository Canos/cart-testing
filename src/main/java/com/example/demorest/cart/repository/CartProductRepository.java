package com.example.demorest.cart.repository;

import com.example.demorest.cart.model.CartProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CartProductRepository extends JpaRepository<CartProduct, Integer> {

    CartProduct findByCart_idCartAndProduct_idProduct(Integer idCart, Integer idProduct);

    void deleteByCart_idCart(Integer idCart);
}
