package com.example.demorest.cart.response;

import com.example.demorest.cart.model.Cart;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;
import java.util.List;

public class CartResponse {

    private Integer idCart;

    @ApiModelProperty(
            value = "Cart user session id",
            notes = "Unique identificator of the user hexadecimal",
            name = "sessionId",
            dataType = "String",
            example = "123434567678899075afe")
    private String sessionId;
    private List<CartProductResponse> products;
    private BigDecimal total;

    public static CartResponse of(Cart cart) {
        if(cart == null) return null;

        CartResponse cartResponse = new CartResponse();
        cartResponse.setIdCart(cart.getIdCart());
        cartResponse.setSessionId(cart.getSessionId());

        cartResponse.setProducts(CartProductResponse.of(cart.getProducts()));

        cartResponse.setTotal(CartProductResponse.getTotalPrice(cart.getProducts()));

        return cartResponse;
    }

    public Integer getIdCart() {
        return idCart;
    }

    public void setIdCart(Integer idCart) {
        this.idCart = idCart;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public List<CartProductResponse> getProducts() {
        return products;
    }

    public void setProducts(List<CartProductResponse> products) {
        this.products = products;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }
}
