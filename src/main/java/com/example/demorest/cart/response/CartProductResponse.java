package com.example.demorest.cart.response;

import com.example.demorest.cart.model.CartProduct;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class CartProductResponse {

    private String product;
    private BigDecimal unitPrice;
    private Integer quantity;

    public static List<CartProductResponse> of(List<CartProduct> products) {
        return products == null || products.isEmpty() ?
                Collections.emptyList() :
                products.stream().map(cp -> CartProductResponse.of(cp)).collect(Collectors.toList());
    }

    public static BigDecimal getTotalPrice(List<CartProduct> products) {

        return products == null || products.isEmpty() ?
                BigDecimal.ZERO :
                products.stream()
                        .map(cp -> cp.getProduct().getPrice().multiply(new BigDecimal(cp.getQuantity())))
                        .reduce((a,v) -> a.add(v)).orElse(BigDecimal.ZERO);
    }

    public static CartProductResponse of(CartProduct cp) {
        CartProductResponse cpr = new CartProductResponse();
        cpr.setProduct(cp.getProduct().getName());
        cpr.setUnitPrice(cp.getProduct().getPrice());
        cpr.setQuantity(cp.getQuantity());

        return cpr;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(BigDecimal unitPrice) {
        this.unitPrice = unitPrice;
    }
}
