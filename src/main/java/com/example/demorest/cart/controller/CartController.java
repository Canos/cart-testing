package com.example.demorest.cart.controller;

import com.example.demorest.cart.model.Cart;
import com.example.demorest.cart.repository.CartRepository;
import com.example.demorest.cart.response.CartResponse;
import com.example.demorest.cart.services.CartService;
import com.example.demorest.catalog.ProductService;
import com.example.demorest.orders.Order;
import com.example.demorest.orders.OrderService;
import com.example.demorest.payment.BankService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;

@RestController
@RequestMapping("/cart")
public class CartController {

    private static final Logger log = LoggerFactory.getLogger(CartController.class);

    @Autowired private CartService cartService;
    @Autowired private ProductService productService;
    @Autowired private CartRepository cartRepository;
    @Autowired private BankService bankService;
    @Autowired private OrderService orderService;

    @GetMapping()
    public ResponseEntity<CartResponse> getOrCreate(HttpSession session) {
        CartResponse cart = cartService.findCart(session.getId());
        if(cart == null) {
            cart = cartService.create(session.getId());
            return new ResponseEntity<>(cart, HttpStatus.CREATED);
        }
        else {
            return new ResponseEntity<>(cart, HttpStatus.OK);
        }
    }

    @PostMapping("/{idCart}/product")
    public ResponseEntity<Void> addProduct(@PathVariable Integer idCart, @RequestParam Integer idProduct, @RequestParam Integer quantity) {
        // comprobar aforo de cada product
        if(!productService.checkStock(idProduct, quantity)) {
            throw new RuntimeException("No hay suficientes productos");
        }

        // añadir al carrito
        cartService.addProduct(idCart, idProduct, quantity);

        log.info("Producto añadido al carrito");
        return ResponseEntity.ok().build();
    }

    @PostMapping("/{idCart}/checkout")
    public ResponseEntity<Void> checkout(@PathVariable Integer idCart, @RequestParam String card) {

        Cart cart = cartRepository.findById(idCart).orElseThrow();


        // comprobar aforo de cada product
        cart.getProducts().forEach(product -> {
            if(!productService.checkStock(product.getProduct().getIdProduct(), product.getQuantity())) {
                throw new RuntimeException("No hay suficientes productos");
            }
        });


        // calcular totales
        final BigDecimal total = cart.getProducts().stream()
                .map(cp -> cp.getProduct().getPrice().multiply(BigDecimal.valueOf(cp.getQuantity())))
                .reduce(BigDecimal.ZERO, (a, price) -> a.add(price));

        // crear el pedido
        Order order = orderService.create(cart.getIdCart(), cart.getProducts(), total);

        // contactar con el banco
        // esperar confirmación banco
        bankService.emulation(order.getReference(), total);

        // restar el stock de cara producto
        cart.getProducts().forEach(product -> {
            productService.reduceStock(product.getProduct().getIdProduct(), product.getQuantity());
        });

        // procesar pedido
        orderService.process(order.getIdOrder());


        // vaciar el carrito para que pueda seguir
        cartService.empty(idCart);

        log.info("Compra terminada");

        return ResponseEntity.ok().build();
    }

}
