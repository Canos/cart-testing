package com.example.demorest.cart.model;

import com.example.demorest.catalog.Product;

import javax.persistence.*;

@Entity
public class CartProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idCartProduct;

    @ManyToOne()
    @JoinColumn(name = "cart")
    private Cart cart;

    @ManyToOne
    @JoinColumn(name = "product")
    private Product product;

    private Integer quantity;

    public Integer getIdCartProduct() {
        return idCartProduct;
    }

    public void setIdCartProduct(Integer idCartProduct) {
        this.idCartProduct = idCartProduct;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

}
